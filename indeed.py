import requests
from bs4 import BeautifulSoup
import pandas


url = 'https://br.indeed.com/jobs?q=programador&l='
response = requests.get(url)
response.text
soup = BeautifulSoup(response.text)
lista = []
for x in range(100):
    for vagas in soup.find_all("div", attrs={"class": "jobsearch-SerpJobCard"}):
        titulo = (vagas.h2.a['title'])
        link = (vagas.h2.a['href'])
       # print('titulo da vaga: {}' .format(titulo))
        #print('link da vaga :{}' .format(link))
       # print('-'*110)
        lista.append([titulo, link])
